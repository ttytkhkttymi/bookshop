<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'min:3',
                'max:50'
            ],
            'login' => [
                'required',
                'string',
                'min:7',
                'max:50'
            ],
            'passowrd' => [
                'request',
                'string',
                'min:5',
                'max:100'
            ],
            'role' => [
                'request',
                'string',
                'min:3',
                'max:50'
            ]
        ];
    }
}
