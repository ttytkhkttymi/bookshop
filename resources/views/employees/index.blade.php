@extends('layout')
@section('content')
    <div class="main-panel" id="main-panel">
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="card-header">
                            <h4>Books</h4>
                        </div>
                    </div>
                    <div class="col-md-6 pl-1">
                        <div class="">
                            <a href="{{ route('employees.create') }}" class="btn btn-primary" style="margin-top: 50px; !important; margin-left: 500px; !important;">
                                Create
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Login</th>
                            <th>Password</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th class="text-right">Action</th>

                            </thead>
                            <tbody>
                            @forelse($employees as $employee)
                                <tr>
                                    <td>{{$employee->id}}</td>
                                    <td>{{$employee->name}}</td>
                                    <td>{{$employee->login}}</td>
                                    <td>{{$employee->password}}</td>
                                    <td>{{$employee->role}}</td>
                                    <td>
                                        @if($employee->status == 1)
                                            <div class="alert alert-success text-white">
                                                ACTIVE
                                            </div>
                                        @else
                                            <div class="alert alert-danger text-white">
                                                DISABLED
                                            </div>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <a class="btn btn-warning" href="{{ route('employees.show', ['employee'=>$employee]) }}">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <a class="btn btn-success" href="{{ route('employees.edit', ['employee'=>$employee]) }}">
                                            <i class="fas fa-pen"></i>
                                        </a>
                                        <form style="display: contents;" method="POST" action="{{route('employees.destroy', ['employee' => $employee])}}">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty

                            @endforelse
                            </tbody>
                        </table>
                        {{--                        {{$admins->links('vendor.pagination.bootstrap-5')}}--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
