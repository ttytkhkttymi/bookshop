@extends('layout')
@section('content')
    <div class="col-md-10 ml-auto mt-3">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Edit Profile</h5>
                @dump($errors->all())
            </div>
            <div class="card-body">
                <form method="POST" action="{{route('orders.update', ['order'=>$order])}}">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>User</label>
                                <select name="user_id" class="form-control">
                                    @foreach($user as $users)
                                        <option value="{{$users->id}}">{{$users->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Book</label>
                                <select name="book_id" class="form-control">
                                    @foreach($book as $books)
                                        <option value="{{$books->id}}">{{$books->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Employee</label>
                                <select name="employee_id" class="form-control">
                                    @foreach($employee as $employees)
                                        <option value="{{$employees->id}}">{{$employees->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <option value="1" @if($book->status == '1') selected @endif>Active</option>
                                    <option value="0" @if($book->status == '0') selected @endif>Disabled</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button class="btn btn-success" style="border-radius:30px; !important" type="submit">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

