@extends('layout')
@section('content')
    <div class="main-panel" id="main-panel">
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="card-header">
                            <h4>Users</h4>
                        </div>
                    </div>
                    <div class="col-md-6 pl-1">
                        <div class="">
                            <a href="{{ route('users.create') }}" class="btn btn-primary" style="margin-top: 50px; !important; margin-left: 500px; !important;">
                                Create
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Passport Number</th>
                            <th>Phone Number</th>
                            <th>Birth Date</th>
                            <th>Status</th>
                            <th class="text-right">Action</th>

                            </thead>
                            <tbody>
                            @forelse($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->passport_number}}</td>
                                    <td>{{$user->phone_number}}</td>
                                    <td>{{$user->birth_date}}</td>
                                    <td>
                                        @if($user->status == 1)
                                            <div class="alert alert-success text-white">
                                                ACTIVE
                                            </div>
                                        @else
                                            <div class="alert alert-danger text-white">
                                                DISABLED
                                            </div>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <a class="btn btn-warning" href="{{ route('users.show', ['user'=>$user]) }}">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <a class="btn btn-success" href="{{ route('users.edit', ['user'=>$user]) }}">
                                            <i class="fas fa-pen"></i>
                                        </a>
                                        <form style="display: contents;" method="POST" action="{{route('users.destroy', ['user' => $user])}}">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty

                            @endforelse
                            </tbody>
                        </table>
{{--                        {{$admins->links('vendor.pagination.bootstrap-5')}}--}}
                    </div>
                </div>
            </div>
        </div>

{{--        <footer class="footer">--}}
{{--            <div class=" container-fluid ">--}}
{{--                <nav>--}}
{{--                    <ul>--}}
{{--                        <li>--}}
{{--                            <a href="#">--}}
{{--                                Creative Tim--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#">--}}
{{--                                About Us--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#">--}}
{{--                                Blog--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </nav>--}}
{{--                <div class="copyright" id="copyright">--}}
{{--                    &copy; <script>--}}
{{--                        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))--}}
{{--                    </script>, Designed by <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </footer>--}}
    </div>
@endsection
